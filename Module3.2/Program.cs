﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool flag;
            flag = int.TryParse(input, out result);
            if (result >= 0 && flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fibonacci = new int[n];
            if (n >= 2)
            {
                fibonacci[0] = 0;
                fibonacci[1] = 1;
            }
            for (int i = 2; i < n; i++)
            {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            }
            return fibonacci;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            bool flag = false;
            if (sourceNumber < 0)
            {
                sourceNumber = Math.Abs(sourceNumber);
                flag = true;
            }
            string input = sourceNumber.ToString();
            string output = new string(input.ToCharArray().Reverse().ToArray());
            if (flag)
            { return int.Parse(output) * (-1); }
            return int.Parse(output);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
            { size = 0; }
            int[] array = new int[size];
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                array[i] = rand.Next(-10, 10);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] *= (-1);
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
            { size = 0; }
            int[] array = new int[size];
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                array[i] = rand.Next(-10, 10);
            }
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> listOfGreaterElements = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i-1] < source[i])
                {
                    listOfGreaterElements.Add(source[i]);
                }
            }
            return listOfGreaterElements;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {

            int[,] matrix = new int[size, size];

            int row = 0;
            int col = 0;
            int dx = 1;
            int dy = 0;
            int dirChanges = 0;
            int visits = size;

            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[row, col] = i + 1;
                if (--visits == 0)
                {
                    visits = size * (dirChanges % 2) + size * ((dirChanges + 1) % 2) - (dirChanges / 2 - 1) - 2;
                    int temp = dx;
                    dx = -dy;
                    dy = temp;
                    dirChanges++;
                }
                col += dx;
                row += dy;
            }
            return matrix;
        }
    }
}
